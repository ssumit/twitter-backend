#!/bin/bash
mvn clean
mvn compile
mvn package
sudo rm -rf /var/lib/tomcat7/webapps/backend/
sudo rm -rf /var/lib/tomcat7/webapps/backend.war
sudo cp ~/backend/target/backend.war /var/lib/tomcat7/webapps/
sudo service apache2 restart
sudo service tomcat7 restart
