package MiniTwitter.DataStore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/10/12
 * Time: 1:13 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CachingI {
    boolean userIdExists(String userId);

    boolean userEmailIdExists(String emailid);

    String getUserIdFromEmail(String emailid);

    boolean checkPassword(String emailid, String password);

    HashMap<String,Object> getUserInfo(String userid);

    HashMap<String,Object> getDetailedUserInfo(String userid);

    boolean follow(String loggedInUserId, String targetUserId);

    boolean unfollow(String loggedInUserId, String targetUserId);

    HashMap<String,Object> getShortProfile(String userId);

    List<Map<String, Object>> search(String query);

    boolean editprofile(String username, String emailid, String password, String userid);

    List<Map<String,Object>> getFeed(String userId, Integer count);

    List<Map<String,Object>> getTweets(String userid, Integer count);

    List<Map<String, Object>> searchTweet(String query);

    List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count);

    List<Map<String, Object>> getTweetsAfter(String userid, String postid);

    List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count);

    List<Map<String, Object>> getFeedAfter(String userId, String feedId);

    String insertTweet(String userId, String userInfo, String postContent);

    void insertUserIdConsistentency(String userId);

    void insertTweetConsistency(String userid);

    void insertFeedAfterInCache(String userId, List<Map<String,Object>> list, String feedId);

    void feedUpdateInCache(String userId, List<Map<String,Object>> list, Integer count);

    void feedDeleteDueToInsertTweet(String followersList);

    void setAndAccessToken(String emailid, int appID, String accessToken);
}
