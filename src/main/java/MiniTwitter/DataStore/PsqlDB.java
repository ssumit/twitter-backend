package MiniTwitter.DataStore;

import MiniTwitter.BackgroundServices.EditProfileHelper;
import MiniTwitter.BackgroundServices.PopulateFeedHelper;
import MiniTwitter.BackgroundServices.reTweetHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/6/12
 * Time: 1:20 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class PsqlDB implements DataStoreI {
    public final JDBCWrapperI db;

    @Autowired
    public PsqlDB(JDBCWrapperI db) {
        this.db = db;
    }

    @Override
    public boolean userIdExists(String userId) {
        Integer count = db.queryForInt("select count(1) from users where userid = ?",userId);
        if(count>0) return true;
        else return false;
    }
    @Override
    public boolean userEmailIdExists(String emailid) {
        Integer count = db.queryForInt("select count(1) from users where emailid = ?",emailid);
        if(count>0) return true;
        else return false;
    }
    @Override
    public String registerUser(String userid, String username, String password, String emailid) {
        String fromDb = db.queryForObject("insert into users(userid,username,password,emailid,followersidlist,followingidlist,followerslist,followinglist) values(?,?,?,?,'','','','') returning userid", String.class, userid, username, password, emailid);
        return fromDb;
    }
    @Override
    public String getUserIdFromEmail(String emailid) {
        if(userEmailIdExists(emailid))
            return db.queryForObject("select userid from users where emailid = ?",String.class,emailid);
        return null;
    }
    @Override
    public boolean checkPassword(String emailid, String password) {
        String dbpassword = db.queryForObject("select password from users where emailid = ?",String.class,emailid);
        if(dbpassword.equals(password)) return true;
        else return false;
    }
    @Override
    public HashMap<String,Object> getUserInfo(String userid) {
        return (HashMap<String,Object>) db.queryForMap("select userid,username,emailid from users where userid = ?", userid);
    }
    @Override
    public HashMap<String,Object> getDetailedUserInfo(String userid) {
        return (HashMap<String,Object>) db.queryForMap("select userid,username,emailid,followerslist,followinglist,postcount,followerscount,followingcount from users where userid = ?", userid);
    }

    @Override
    public boolean follow(String loggedInUserId, String targetUserId) {
        String followersList = db.queryForObject("select followersidlist from users where userid = ?", String.class, targetUserId);
        if(followersList.length()>0) for(String user: followersList.split(",")) {
            if(loggedInUserId.equals(user))
                return false;
        }
        db.update("UPDATE users SET followerslist = trim(leading ',' from followerslist || ?), followersidlist = trim(leading ',' from followersidlist || ?), followerscount = followerscount + 1 where userid = ?", ","+getUserInfoString(loggedInUserId), ","+loggedInUserId, targetUserId);
        db.update("UPDATE users SET followinglist = trim(leading ',' from followinglist || ?), followingidlist = trim(leading ',' from followingidlist || ?), followingcount = followingcount + 1 where userid = ?", ","+getUserInfoString(targetUserId),","+targetUserId,loggedInUserId);
        return true;
    }
    @Override
    public String getUserInfoString(String userid) {
        return new JSONObject(getUserInfo(userid)).toString();
    }


    @Override
    public boolean unfollow(String loggedInUserId, String targetUserId) {
        if (loggedInUserId.equals(targetUserId)) return false;
        String followersIdList = db.queryForObject("select followersidlist from users where userid = ?", String.class, targetUserId);
        boolean found = false;
        String newFollowersIdList = "";
        if(followersIdList.length()>0) for (String user : followersIdList.split(",")) {
            if (loggedInUserId.equals(user))
                found = true;
            else
                newFollowersIdList += user + ",";
        }
        if (!found)
            return false;
        if(newFollowersIdList.length()>0)
            newFollowersIdList = newFollowersIdList.substring(0, newFollowersIdList.length() - 1);
        String followingIdList = db.queryForObject("select followingidlist from users where userid = ?",String.class,loggedInUserId);
        String newFollowingIdList = "";
        if(followingIdList.length()>0) for(String user:followingIdList.split(",")) {
            if(!user.equals(targetUserId))
                newFollowingIdList += user + ",";
        }
        if(newFollowingIdList.length()>0)
            newFollowingIdList = newFollowingIdList.substring(0,newFollowingIdList.length()-1);
        String followersList = db.queryForObject("select followerslist from users where userid = ?", String.class, targetUserId);
        JSONArray followersListJSON = null;
        try {
            followersListJSON = new JSONArray("["+followersList+"]");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        String newFollowersList = remove(followersListJSON, loggedInUserId);
        db.update("update users set followersidlist = ?, followerslist = ?, followerscount = followerscount - 1 where userid = ?", newFollowersIdList, newFollowersList, targetUserId);
        String followingList = db.queryForObject("select followinglist from users where userid = ?", String.class, loggedInUserId);
        JSONArray followingListJSON = null;
        try {
            followingListJSON = new JSONArray("["+followingList+"]");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        String newFollowingList = remove(followingListJSON, targetUserId);
        db.update("update users set followingidlist = ?, followinglist = ?, followingcount = followingcount - 1 where userid = ?", newFollowingIdList, newFollowingList, loggedInUserId);
        return true;
    }

    String remove(JSONArray userList, String userId) {
        JSONArray newList = new JSONArray();
        for(int i=0;i<userList.length();i++) {
            JSONObject user = null;
            try {
                user = userList.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                if(!user.getString("userid").equals(userId))
                    newList.put(user);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        String response = newList.toString();
        int till = response.length()-1;
        if(till>0)
            return response.substring(1,till);
        else
            return "";
    }

    @Override
    public HashMap<String,Object> getShortProfile(String userId) {
        if(!userIdExists(userId))
            return new HashMap<String, Object>();
        return (HashMap<String,Object>) db.queryForMap("select userid,username,emailid,postcount,followerscount,followingcount from users where userid = ?",userId);
    }
    @Override
    public List<Map<String, Object>> search(final String query) {
        final List<Map<String, Object>> users = db.queryForList("select userid,username,emailid,postcount,followerscount,followingcount from users where username ilike ? or emailid ilike ?", "%"+query+"%", "%"+query+"%");
        return users;
    }

    @Override
    public boolean editprofile(String username, String emailid, String password, String userid) {
        //save it in users table actual columns
        db.update("update users set username = ?, emailid = ?, password = ? where userid = ?",username,emailid,password,userid);
        String userInfo = getUserInfoString(userid);
        //save it in post table sourceuser column matching userid
        db.update("update post set sourceuser = ? where userid = ?",userInfo,userid);
        //save it in feeds table sourceuser column matching sourceuserid
        db.update("update feeds set sourceuser = ? where sourceuserid = ?",userInfo,userid);
        //save it in users table followerslist and followinglist, since it might be a heavy process span a new thread
        new EditProfileHelper(this,userid,userInfo).start();
        return true;
    }

    @Override
    public List<Map<String,Object>> getFeed(String userid, Integer count) {
        return db.queryForList("select * from feeds where feeduserid = ? order by feedid desc limit ?",userid,count);
    }
    @Override
    public List<Map<String,Object>> getTweets(String userid, Integer count) {
        return db.queryForList("select * from post where userid = ? order by postid desc limit ?",userid,count);
    }
    @Override
    public List<Map<String,Object>> searchTweet(String query) {
        return db.queryForList("select * from post where postcontent ilike ?","%"+query+"%");
    }
    @Override
    public List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count) {
        return db.queryForList("select * from feeds where feeduserid = ? AND feedid < ? order by feedid desc limit ?",userid,new Integer(feedId),count);
    }
    @Override
    public List<Map<String, Object>> getFeedAfter(String userid, String feedId) {
        return db.queryForList("select * from feeds where feeduserid = ? AND feedid > ? order by feedid desc", userid,new Integer(feedId));
    }

    @Override
    public List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count) {
        return db.queryForList("select * from post where userid = ? AND postid < ? order by postid desc limit ?",userid,new Integer(postid),count);
    }

    @Override
    public List<Map<String, Object>> getTweetsAfter(String userid, String postid) {
        return db.queryForList("select * from post where userid = ? AND postid > ? order by postid desc", userid,new Integer(postid));
    }

    @Override
    public String insertTweet(String userId, String userInfo, String postContent){
        db.update("update users set postcount = postcount + 1 where userid = ?",userId);
        Integer tweetid = db.queryForInt("insert into post (userId, sourceuser, postContent) values(?,?,?) returning postid", userId, userInfo, postContent);
        String postid = tweetid.toString();
        db.update("insert into feeds (timestamp,feeduserid, sourceuserid, sourceuser, postid, postcontent) values(now(),?,?,?,?,?)", userId, userId, userInfo, postid, postContent);
        new PopulateFeedHelper(this,userId,userInfo,postid,postContent).start();
        return postid;
    }
    @Override
    public String getQueryForObject(String sql, Class <String> requiredType,Object... args) {
        return (String) db.queryForObject(sql,requiredType, args);
    }

    @Override
    public int doUpdate(String sql, Object... args){
        return db.update(sql,args);
    }

    @Override
    public Map<String, Object> getPost(String postid) {
        if(db.queryForInt("select count(1) from post where postid = ?",new Integer(postid))==0)
            return null;
        return db.queryForMap("select * from post where postid = ?",new Integer(postid));
    }

    @Override
    public void retweet(String retweetingUserId, Timestamp timestamp, String sourceuserid, String sourceuser, String postcontent, String postid) {
        new reTweetHelper(this,retweetingUserId,timestamp,sourceuserid,sourceuser,postcontent,postid).start();
    }

    @Override
    public void insertIntoFeed(Timestamp timestamp, String retweetingUserId, String sourceuser, Object postcontent, String postid, String sourceuserid) {
        db.update("delete from feeds where feeduserid = ? and postid = ?",retweetingUserId,postid);
        db.update("insert into feeds (timestamp,feeduserid,sourceuser,postcontent,postid,sourceuserid) values(?,?,?,?,?,?)",timestamp,retweetingUserId,sourceuser,postcontent,postid,sourceuserid);
    }

    @Override
    public void setAndAccessToken(String emailid, int appID, String accessToken) {
        int count = db.queryForInt("select count(1) from aouthtokens where emailid = ? and appid=?",emailid,appID);
        if(count>0){
            db.update("update aouthtokens set (accessToken,timestamp)=(?,now()) where emailid=? and appid=?",accessToken,emailid,appID);
        }
        else
            db.update("insert into aouthtokens (emailid,appid,accesstoken,timestamp) values(?,?,?,now())",emailid,appID,accessToken);
    }

    @Override
    public int registerApp(String modulus, int exp, String appName) {
        return db.queryForInt("insert into aouthregister (modulus,exp,appname) values(?,?,?) returning appid", modulus, exp, appName);
    }

    @Override
    public Map<String,Object> getPublicKey(int appID) {
        return db.queryForMap("select modulus,exp from aouthregister where appid=?", appID);
    }

    @Override
    public String addTweet(String tweet, String accessToken) {
        String userid=null;
        try{
            Timestamp timestamp= (Timestamp) db.queryForObject("select timestamp from aouthtokens where accesstoken=?",Timestamp.class,accessToken);
            java.util.Date date= new java.util.Date();
            Timestamp currentTimestamp = new Timestamp(date.getTime());
            if(diffGreaterThan2Days((Timestamp) timestamp, currentTimestamp)){
                db.update("delete from aouthtokens where accesstoken=?",accessToken);
                return null;
            }
            userid =(String) db.queryForObject("select userid from users where emailid in (select emailid from aouthtokens where accesstoken=?)",String.class,accessToken);
        }
        catch (Exception e){
            return null;
        }
        if(userid!=null)
        insertTweet(userid,getUserInfoString(userid),tweet);
        return userid;
    }

    @Override
    public String getPassword(String emailid) {
        return (String) db.queryForObject("select password from users where emailid=?",String.class,emailid);
    }

    @Override
    public void markConfirmMail(String emailid) {
        db.update("update users set emailverification=true where emailid=?",emailid);
    }

    @Override
    public boolean emailVerified(String emailid) {
        String emailverification = (String) db.queryForObject("select emailverification from users where emailid=?",String.class,emailid);
        return emailverification.equals("t");
    }
    @Override
    public String getFollowersList(String userid) {
        return (String) db.queryForObject("select followerslist from users where userid = ?", String.class,userid);
    }
    @Override
    public String getFollowingList(String userid) {
        return (String) db.queryForObject("select followinglist from users where userid = ?", String.class, userid);
    }

    @Override
    public List<Map<String,Object>> getAllTweets(String userid) {
        return  (List)db.queryForList("select * from post where userid=?",userid);
    }

    @Override
    public String getPasswordByUserId(String userid) {
        return (String) db.queryForObject("select password from users where userid=?",String.class,userid);
    }

    @Override
    public boolean getValidityOfAccessToken(String accessToken) {
        try{
            Timestamp timestamp= (Timestamp) db.queryForObject("select timestamp from aouthtokens where accesstoken=?",Timestamp.class,accessToken);
            java.util.Date date= new java.util.Date();
            Timestamp currentTimestamp = new Timestamp(date.getTime());
            if(diffGreaterThan2Days((Timestamp) timestamp, currentTimestamp)){
                db.update("delete from aouthtokens where accesstoken=?",accessToken);
                return false;
            }
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @Override
    public String getUserIdByAccessToken(String accessToken) {
        return (String) db.queryForObject("select userid from users where emailid in (select emailid from aouthtokens where accesstoken=?)",String.class,accessToken);
    }

    @Override
    public String getEmailFromUserId(String userid) {
        return (String) db.queryForObject("select emailid from users where userid=?",String.class,userid);
    }

    private boolean diffGreaterThan2Days(Timestamp timestamp, Timestamp timestamp1) {
        return false;  //To change body of created methods use File | Settings | File Templates.
    }

}
