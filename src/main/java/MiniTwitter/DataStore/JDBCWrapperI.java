package MiniTwitter.DataStore;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/10/12
 * Time: 2:02 PM
 * To change this template use File | Settings | File Templates.
 */
public interface JDBCWrapperI {
    int queryForInt(String sql, Map<String, ?> args);

    int queryForInt(String sql, SqlParameterSource args);

    int queryForInt(String sql,  Object... args);

    <T> T queryforObject(String sql, Class<T> requiredtype, Map<String, ?> args);

    <T> T queryForObject(String sql, Class<T> requiredType, SqlParameterSource args);

    <T> T queryForObject(String sql, Class<T> requiredType, Object... args);

    Map<String, Object> queryForMap(String sql, Map<String, ?> args);

    Map<String, Object> queryForMap(String sql, SqlParameterSource args);

    Map<String, Object> queryForMap(String sql, Object... args);

    int update(String sql, Map<String, ?> args);

    int update(String sql, SqlParameterSource args);

    int update(String sql, Object... args);

    List<Map<String, Object>> queryForList(String sql, Map<String, ?> args);

    List<Map<String, Object>> queryForList(String sql, SqlParameterSource args);

    List<Map<String, Object>> queryForList(String sql, Object... args);
}
