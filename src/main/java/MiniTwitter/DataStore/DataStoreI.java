package MiniTwitter.DataStore;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/9/12
 * Time: 10:07 PM
 * To change this template use File | Settings | File Templates.
 */
public interface DataStoreI {
    boolean userIdExists(String userId);

    boolean userEmailIdExists(String emailid);

    String registerUser(String userid, String username, String password, String emailid);

    String getUserIdFromEmail(String emailid);

    boolean checkPassword(String emailid, String password);

    HashMap<String,Object> getUserInfo(String userid);

    HashMap<String,Object> getDetailedUserInfo(String userid);

    boolean follow(String loggedInUserId, String targetUserId);

    String getUserInfoString(String userid);

    boolean unfollow(String loggedInUserId, String targetUserId);

    HashMap<String,Object> getShortProfile(String userId);

    List<Map<String, Object>> search(String query);

    boolean editprofile(String username, String emailid, String password, String userid);

    List<Map<String,Object>> getFeed(String userid, Integer count);

    List<Map<String,Object>> getTweets(String userid, Integer count);

    List<Map<String,Object>> searchTweet(String query);

    List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count);

    List<Map<String, Object>> getFeedAfter(String userid, String feedId);

    List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count);

    List<Map<String, Object>> getTweetsAfter(String userid, String postid);

    String insertTweet(String userId, String userInfo, String postContent);

    String getQueryForObject(String sql, Class<String> requirdType, Object... args);

    int doUpdate(String sql, Object... args);

    Map<String,Object> getPost(String postid);

    void retweet(String retweetingUserId, Timestamp timestamp, String sourceuserid, String sourceuser, String postcontent, String postid);

    void insertIntoFeed(Timestamp timestamp, String retweetingUserId, String sourceuser, Object postcontent, String postid, String sourceuserid);

    void setAndAccessToken(String emailid, int appID, String accessToken);

    int registerApp(String modulus,int exp, String appName);

    Map<String,Object> getPublicKey(int appID);

    String addTweet(String tweet, String accessToken);

    String getPassword(String emailid);

    void markConfirmMail(String emailid);

    boolean emailVerified(String emailid);

    String getFollowersList(String userid);

    String getFollowingList(String userid);

    List<Map<String,Object>> getAllTweets(String userid);

    String getPasswordByUserId(String userid);

    boolean getValidityOfAccessToken(String accessToken);

    String getUserIdByAccessToken(String accessToken);

    String getEmailFromUserId(String userid);
}
