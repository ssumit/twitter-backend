package MiniTwitter.DataStore;

import MiniTwitter.Constants;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/10/12
 * Time: 1:39 PM
 * To change this template use File | Settings | File Templates.
 */

public class JDBCWrapper implements JDBCWrapperI {

    private SimpleJdbcTemplate simpleJdbcTemplate;

    public JDBCWrapper() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setUrl(Constants.PSQL_URL);
        basicDataSource.setDriverClassName(Constants.PSQL_DRIVER_CLASS);
        basicDataSource.setUsername(Constants.PSQL_USERNAME);
        basicDataSource.setPassword(Constants.PSQL_PASSWORD);

        this.simpleJdbcTemplate = new SimpleJdbcTemplate(basicDataSource);
    }
    @Override
    public int queryForInt(String sql, Map<String, ?> args){

        return simpleJdbcTemplate.queryForInt(sql,args);
    }
    @Override
    public int queryForInt(String sql, SqlParameterSource args){
        return simpleJdbcTemplate.queryForInt(sql,args);
    }
    @Override
    public int queryForInt(String sql, Object... args){
        return simpleJdbcTemplate.queryForInt(sql,args);
    }
    @Override
    public <T> T queryforObject(String sql, Class<T> requiredtype, Map<String, ?> args) {
        return simpleJdbcTemplate.queryForObject(sql,requiredtype,args);
    }
    @Override
    public <T> T queryForObject(String sql, Class<T> requiredType, SqlParameterSource args) {
        return simpleJdbcTemplate.queryForObject(sql, requiredType, args);
    }
    @Override
    public <T> T queryForObject(String sql, Class<T> requiredType, Object... args){
        return simpleJdbcTemplate.queryForObject(sql,requiredType,args);
    }
    @Override
    public Map<String, Object> queryForMap(String sql, Map<String, ?> args) {
        return simpleJdbcTemplate.queryForMap(sql, args);
    }
    @Override
    public Map<String, Object> queryForMap(String sql, SqlParameterSource args) {
        return simpleJdbcTemplate.queryForMap(sql,args);
    }
    @Override
    public Map<String, Object> queryForMap(String sql, Object... args){
        return simpleJdbcTemplate.queryForMap(sql,args);
    }
    @Override
    public int update(String sql, Map<String, ?> args) {
        return simpleJdbcTemplate.update(sql,args);
    }
    @Override
    public int update(String sql, SqlParameterSource args) {
        return simpleJdbcTemplate.update(sql,args);
    }
    @Override
    public int update(String sql, Object... args){
        return simpleJdbcTemplate.update(sql,args);
    }
    @Override
    public List<Map<String, Object>> queryForList(String sql, Map<String, ?> args){
        return simpleJdbcTemplate.queryForList(sql, args);
    }
    @Override
    public List<Map<String, Object>> queryForList(String sql, SqlParameterSource args){
        return simpleJdbcTemplate.queryForList(sql,args);
    }
    @Override
    public List<Map<String, Object>> queryForList(String sql, Object... args) {
        return simpleJdbcTemplate.queryForList(sql,args);
    }
}
