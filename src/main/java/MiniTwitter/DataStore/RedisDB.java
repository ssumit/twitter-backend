package MiniTwitter.DataStore;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/6/12
 * Time: 1:19 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class RedisDB implements CachingI {
    public final JedisPool jedisPool;

    @Autowired
    public RedisDB(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    @Override
    public boolean userIdExists(String userId) {
        Jedis jedis = jedisPool.getResource();
        if(jedis.exists(userId+"exists"))  {
            jedis.expire(userId+"exists",120);
            jedisPool.returnResource(jedis);
            return true;
        }
        else return false;
    }
    @Override
    public boolean userEmailIdExists(String emailid) {
        return false;
    }

    @Override
    public String getUserIdFromEmail(String emailid) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }
    @Override
    public boolean checkPassword(String emailid, String password) {
        return false;
    }
    @Override
    public HashMap<String,Object> getUserInfo(String userid) {
        return null;
    }
    @Override
    public HashMap<String,Object> getDetailedUserInfo(String userid) {
        return null;
    }

    @Override
    public boolean follow(String loggedInUserId, String targetUserId) {
        return false;
    }
    @Override
    public boolean unfollow(String loggedInUserId, String targetUserId) {
        return false;
    }
    @Override
    public HashMap<String,Object> getShortProfile(String userId) {
        return null;
    }
    @Override
    public List<Map<String, Object>> search(final String query) {
        return null;
    }

    @Override
    public boolean editprofile(String username, String emailid, String password, String userid) {
        return false;
    }
    @Override
    public List<Map<String,Object>> getFeed(String userId, Integer count) {
        Jedis jedis = jedisPool.getResource();
        if(!jedis.exists(userId+"feeds")) {
            jedisPool.returnResource(jedis);
            return null;
        }
        Set<String> stringSet = jedis.zrange(userId+"feeds", 0, 1000);
        ArrayList<Map<String,Object>> hashMapList = new ArrayList<Map<String, Object>>();
        for (String s : stringSet) {
            String string = removeBackslashFromString(s);
            hashMapList.add(stringToMap(string));
        }
        jedisPool.returnResource(jedis);
        return hashMapList;  //To change body of created methods use File | Settings | File Templates.
    }
    @Override
    public List<Map<String,Object>> getTweets(String userid, Integer count) {
        return null;
    }


    @Override
    public List<Map<String, Object>> searchTweet(String query) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    @Override
    public List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    @Override
    public List<Map<String, Object>> getTweetsAfter(String userid, String postid) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    @Override
    public List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count) {
        return null;  //To change body of created methods use File | Settings | File Templates.
    }

    @Override
    public List<Map<String, Object>> getFeedAfter(String userId, String feedId) {
        Jedis jedis = jedisPool.getResource();
        if(!jedis.exists(userId+"feedafter"+feedId)) {
            jedisPool.returnResource(jedis);
            return null;
        }
        Set<String> stringSet = jedis.zrange(userId+"feedafter"+feedId, 0, 1000);
        ArrayList<Map<String,Object>> hashMapList = new ArrayList<Map<String, Object>>();
        for (String s : stringSet) {
            String string = removeBackslashFromString(s);
            hashMapList.add(stringToMap(string));
        }
        jedisPool.returnResource(jedis);
        return hashMapList;  //To change body of created methods use File | Settings | File Templates.
    }

    private Map<String, Object> stringToMap(String string) {

        Map<String, Object> map = new HashMap<String, Object>();
        for (String s : string.split(",")) {
            String[] temp = s.split(":");
            String value = s.substring(temp[0].length());
            if(value.charAt(0) == '\"') {
                value = value.substring(1,value.length()-1);
            }
            map.put(temp[0],value);
        }
        return map;
    }

    @Override
    public String insertTweet(String userId, String userInfo, String postContent){
        return null;
    }

    @Override
    public void insertUserIdConsistentency(String userId) {
        Jedis jedis = jedisPool.getResource();
        jedis.set(userId + "exists", "1");
        jedis.expire(userId + "exists", 12000);
        jedisPool.returnResource(jedis);
    }

    @Override
    public void insertTweetConsistency(String userid) {
        Jedis jedis = jedisPool.getResource();
        jedis.del(userid + "feed");
        jedisPool.returnResource(jedis);
    }

    @Override
    public void insertFeedAfterInCache(String userId, List<Map<String, Object>> list, String feedId) {
        Jedis jedis = jedisPool.getResource();
        for (Map<String, Object> map : list) {
            jedis.zadd(userId+"feedafter"+feedId,1, new JSONObject(map).toString());
        }
        jedis.expire(userId + "feedafter" + feedId, 120);
        jedisPool.returnResource(jedis);

    }

    @Override
    public void feedUpdateInCache(String userId, List<Map<String, Object>> list, Integer count) {
        Jedis jedis = jedisPool.getResource();
        for (Map<String, Object> map : list) {
            jedis.zadd(userId+"feed",1, new JSONObject(map).toString());
        }
        jedis.expire(userId+"feed",40);
        jedisPool.returnResource(jedis);
    }

    @Override
    public void feedDeleteDueToInsertTweet(String followersList) {
        Jedis jedis = jedisPool.getResource();
        if(followersList.length()>0) for(String user: followersList.split(",")) {
            jedis.del(user+"feed");
        }
        jedisPool.returnResource(jedis);
    }

    @Override
    public void setAndAccessToken(String emailid, int appID, String accessToken) {
        Jedis jedis = jedisPool.getResource();
        String appIDString =new Integer(appID).toString();
        jedis.set(appIDString+emailid,accessToken);
        jedis.expire(appIDString+emailid,3600);
        jedisPool.returnResource(jedis);
    }

    private String removeBackslashFromString(String string) {
        char[] temp = new char[300];
        int k=0;
        int length = string.length();
        for (int i=0;i<length;i++) {
            if(string.charAt(i)=='\\') {
                if(i+1<length) {
                    temp[k]=string.charAt(i+1);
                    k++;
                }
                i++;
            }
            else {
                temp[k] = string.charAt(i);
                k++;
            }
        }
        return new String(temp);
    }
}
