package MiniTwitter.services;

import MiniTwitter.Constants;
import MiniTwitter.DataAccess.OAuthDataAccess;
import MiniTwitter.DataAccess.UserDataStore;
import net.tanesha.recaptcha.ReCaptchaImpl;
import net.tanesha.recaptcha.ReCaptchaResponse;
import org.apache.commons.codec.binary.Hex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.*;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/9/12
 * Time: 10:21 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class MiscUtilities {
    public final OAuthDataAccess oAuthDataAccess;
    public final Users users;
    public final UserDataStore userDataStore;

    @Autowired
    public MiscUtilities(OAuthDataAccess oAuthDataAccess, Users users, UserDataStore userDataStore) {
        this.oAuthDataAccess = oAuthDataAccess;
        this.users = users;
        this.userDataStore = userDataStore;
    }

    public boolean isCaptchaValid(String recaptcha_challenge_field, String recaptcha_response_field) {
        ReCaptchaImpl reCaptcha = new ReCaptchaImpl();
        reCaptcha.setPrivateKey(Constants.RECAPTCHA_PRIVATE_KEY);
        ReCaptchaResponse reCaptchaResponse = reCaptcha.checkAnswer(Constants.LOCALHOST, recaptcha_challenge_field, recaptcha_response_field);
        return reCaptchaResponse.isValid();
    }
    public String encrypt(String message, String modulus,int exp) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, InvalidKeySpecException {
        RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger(modulus),BigInteger.valueOf(exp));
        Key publicKey = KeyFactory.getInstance("RSA").generatePublic(spec);

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] bytes = message.getBytes("UTF-8");

        byte[] encrypted = blockCipher(cipher,bytes,Cipher.ENCRYPT_MODE);

        char[] encryptedTranspherable = Hex.encodeHex(encrypted);
        return new String(encryptedTranspherable);
    }
    public String decrypt(String encrypted,Key key) throws Exception{
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] bts = encrypted.getBytes("UTF-8");//Hex.decodeHex(encrypted.toCharArray()); //
        byte[] decrypted = blockCipher(cipher,bts,Cipher.DECRYPT_MODE);

        String paddedString = new String(decrypted,"UTF-8");
        return removePadding(paddedString);
    }

    private String removePadding(String paddedString) {
        paddedString = paddedString.split("\\\u0000")[0];
        return paddedString;
    }

    private byte[] blockCipher(Cipher cipher, byte[] bytes, int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {

        byte[] scrambled = new byte[0];

        byte[] toReturn = new byte[0];
        int length = (mode == Cipher.ENCRYPT_MODE)? 100 : 128;

        byte[] buffer = new byte[length];

        int i;
        for ( i=0; i< bytes.length; i++){

            if ((i > 0) && (i % length == 0)){
                scrambled = cipher.doFinal(buffer);
                toReturn = append(toReturn,scrambled);

                int newlength = length;

                if (i + length > bytes.length) {
                    newlength = bytes.length - i;
                }
                buffer = new byte[newlength];
            }
            buffer[i%length] = bytes[i];
        }

        scrambled = cipher.doFinal(buffer);
        toReturn = append(toReturn,scrambled);
        return toReturn;
    }

    private byte[] append(byte[] prefix, byte[] suffix){
        byte[] toReturn = new byte[prefix.length + suffix.length];
        for (int i=0; i< prefix.length; i++){
            toReturn[i] = prefix[i];
        }
        for (int i=0; i< suffix.length; i++){
            toReturn[i+prefix.length] = suffix[i];
        }
        return toReturn;
    }

    public String setAndGetAccessToken(String emailid, int appID) {
        return oAuthDataAccess.setAndGetAccessToken(emailid,appID);
    }

    public int registerApp(String modulus,int exp, String appName) {
        return oAuthDataAccess.registerApp(modulus,exp,appName);
    }

    public Key getPublicKey(int appID) throws NoSuchAlgorithmException, InvalidKeySpecException {
        Map<String,Object> map= oAuthDataAccess.getPublicKey(appID);
        RSAPrivateKeySpec spec2 = new RSAPrivateKeySpec(new BigInteger(Constants.public_key_mod), new BigInteger(Constants.private_key_exp));
//        RSAPublicKeySpec spec = new RSAPublicKeySpec(new BigInteger((String) map.get("modulus")), new BigInteger(map.get("exp").toString()));
        Key key = KeyFactory.getInstance("RSA").generatePrivate(spec2);
        return key;
    }

    public Boolean confirmEmail(String emailid, String hash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String calculatedHash =  users.getURL(emailid);
        if(calculatedHash.equals(hash)){
            return true;
        }
        return false;
    }

    public void copyFile(String source, String destination) throws IOException {
        File sourceFile = new File(source);
        File destinationFile = new File(destination);
        InputStream in = new FileInputStream(sourceFile);

        OutputStream out = new FileOutputStream(destinationFile);

        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0){
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }


    public static class EmailValidator{

        private Pattern pattern;
        private Matcher matcher;
        public EmailValidator(){
            pattern = Pattern.compile(Constants.EMAIL_PATTERN);
        }
        public boolean validate(final String hex){

            matcher = pattern.matcher(hex);
            return matcher.matches();

        }
    }

    public void markConfirmMail(String emailid) {
        userDataStore.markConfirmMail(emailid);
    }

}
