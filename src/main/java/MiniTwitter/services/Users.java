package MiniTwitter.services;

import MiniTwitter.Constants;
import MiniTwitter.DataAccess.UserDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jack
 * Date: 23/7/12
 * Time: 4:04 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class Users {
    public final Tweets tweets;
    public final UserDataStore userDataStore;

    @Autowired
    public Users(Tweets tweets, UserDataStore userDataStore) {
        this.tweets = tweets;
        this.userDataStore = userDataStore;
    }

    public HashMap<String,Object> loginResponse = new HashMap<String, Object>() {{
        put("message", Constants.USER_NOT_LOGGEDIN);
        put("display",Constants.RESPONSE_LOGIN);
    }};

    public boolean checkUserLogin(HttpSession session) {
        String userId = (String) session.getAttribute("userid");
        if(userId==null||!userIdExists(userId)) return false;
        else return true;
    }

    public String getLoggedInUserId(HttpSession session) {
        String userId = (String) session.getAttribute("userid");
        if(userId==null||!userIdExists(userId)) return null;
        else return userId;
    }

    public boolean userIdExists(String userId) {
        return userDataStore.userIdExists(userId);
    }

    public boolean userEmailIdExists(String emailid) {
        return userDataStore.userEmailIdExists(emailid);
    }

    public String registerUser(String userid, String username, String password, String emailid) {
        return userDataStore.registerUser(userid,username,password,emailid);
    }

    public String getUserIdFromEmail(String emailid) {
        return userDataStore.getUserIdFromEmail(emailid);
    }

    public boolean checkPassword(String emailid, String password) {
        return userDataStore.checkPassword(emailid,password);
    }

    public void login(String userid, HttpSession session) {
        session.setAttribute("userid",userid);
    }


    public void logout(HttpSession session) {
        session.invalidate();
    }

    public HashMap<String,Object> getUserInfo(String userid) {
        return userDataStore.getUserInfo(userid);
    }

    public HashMap<String,Object> getDetailedUserInfo(String userid) {
        return userDataStore.getDetailedUserInfo(userid);
    }

    public boolean follow(String loggedInUserId, String targetUserId) {
        if(loggedInUserId.equals(targetUserId)) return false;
        return userDataStore.follow(loggedInUserId,targetUserId);
    }

    public boolean unfollow(String loggedInUserId, String targetUserId) {
        return userDataStore.unfollow(loggedInUserId,targetUserId);
    }

    public HashMap<String, Object> getProfile(String userId) {
        if(!userIdExists(userId))
            return new HashMap<String, Object>();
        HashMap<String,Object> profile = getDetailedUserInfo(userId);
        profile.put("followerslist","["+profile.get("followerslist")+"]");
        profile.put("followinglist","["+profile.get("followinglist")+"]");
        profile.put("tweets",tweets.getTweets(userId,10));
        return profile;
    }

    public HashMap<String,Object> getShortProfile(String userId) {
        return userDataStore.getShortProfile(userId);
    }

    public List<Map<String, Object>> search(final String query) {
        return userDataStore.search(query);
    }

    public boolean editprofile(String username, String emailid, String password, String userid) {
        return userDataStore.editprofile(username,emailid,password,userid);
    }

    public String getPassword(String emailid) {
        return userDataStore.getPassword(emailid);
    }

    public String getPasswordByUserid(String userid) {
        return userDataStore.getPasswordByUserid(userid);
    }

    public String getURL(String emailid) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        byte [] bytes = (getPassword(emailid)+emailid).getBytes("UTF-8");
        byte [] hashedbytes = messageDigest.digest(bytes);
        BigInteger bigInt = new BigInteger(1,hashedbytes);
        return bigInt.toString(16);
    }

    public boolean emailVerified(String emailid) {
        return userDataStore.emailVerified(emailid);
    }
    public String getFollowersList(String userid) {
        return userDataStore.getFollowersList(userid);
    }

    public String getFollowingList(String userid) {
        return userDataStore.getFollowingList(userid);
    }

    public List<Map<String,Object>> getAllTweets(String userid) {
        return userDataStore.getAllTweets(userid);
    }

    public String getEmailFromUserId(String userid) {
        return userDataStore.getEmailFromUserId(userid);
    }
}
