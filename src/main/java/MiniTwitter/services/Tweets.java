package MiniTwitter.services;

import MiniTwitter.DataAccess.TweetDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jack
 * Date: 23/7/12
 * Time: 4:43 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class Tweets {
    public final TweetDataStore tweetDataStore;
    @Autowired
    public Tweets(TweetDataStore tweetDataStore) {
        this.tweetDataStore = tweetDataStore;
    }


    public List<Map<String,Object>> getFeed(String userid, Integer count) {
        return tweetDataStore.getFeed(userid,count);
    }

    public List<Map<String,Object>> getTweets(String userid, Integer count) {
        return tweetDataStore.getTweets(userid,count);
    }

    public List<Map<String,Object>> search(String query) {
        return tweetDataStore.search(query);
    }

    public List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count) {
        return tweetDataStore.getFeedBefore(userid,feedId,count);
    }

    public List<Map<String, Object>> getFeedAfter(String userid, String feedId) {
        return tweetDataStore.getFeedAfter(userid,feedId);
    }

    public List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count) {
        return tweetDataStore.getTweetsBefore(userid, postid, count);
    }

    public List<Map<String, Object>> getTweetsAfter(String userid, String postid) {
        return tweetDataStore.getTweetsAfter(userid, postid);
    }

    public String insertTweet(String userid, String userInfo, String postcontent) {
        return tweetDataStore.insertTweet(userid, userInfo,postcontent);
    }

    public Map<String,Object> getPost(String postid) {
        return tweetDataStore.getPost(postid);
    }

    public void retweet(String retweetingUserId, String postid) {
        Map<String,Object> post = getPost(postid);
        Timestamp timestamp = (Timestamp) post.get("timestamp");
        String sourceuserid = (String) post.get("userid");
        String sourceuser = (String) post.get("sourceuser");
        String postcontent = post.get("postcontent")+"\nRetweet by @"+ retweetingUserId;
        tweetDataStore.insertIntoFeed(timestamp,retweetingUserId,sourceuser,post.get("postcontent"),postid,sourceuserid);
        tweetDataStore.retweet(retweetingUserId,timestamp,sourceuserid,sourceuser,postcontent,postid);
    }
}
