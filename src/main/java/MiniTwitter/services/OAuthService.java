package MiniTwitter.services;

import MiniTwitter.DataAccess.OAuthDataAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/15/12
 * Time: 1:05 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OAuthService {
    public final OAuthDataAccess oAuthDataAccess;

    @Autowired
    public OAuthService(OAuthDataAccess oAuthDataAccess) {
        this.oAuthDataAccess = oAuthDataAccess;
    }

    public boolean postTweet(String tweet, String accessToken) {
        return oAuthDataAccess.postTweet(tweet,accessToken);
    }

    public List<Map<String, Object>> getAllNewsFeed(String accessToken) {
        return oAuthDataAccess.getAllNewsFeed(accessToken);
    }
}
