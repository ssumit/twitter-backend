package MiniTwitter.services;

import MiniTwitter.Constants;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;


/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/15/12
 * Time: 5:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class JavaMailingService extends Thread{
    private final String receiver;
    private final String url;
    public JavaMailingService(String receiver, String url) {
        this.receiver = receiver;
        this.url = url;
    }

    @Override
    public void run(){
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", "465");

            Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Constants.EMAIL_VERIFICATION_USERID, Constants.EMAIL_VERIFICATION_PASSWORD);
                    }
                });

            try {

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("dontreplybackminitwitter@gmail.com"));
                message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(receiver));
                message.setSubject("Confirm Email Subject");
                message.setText("Dear please confirm your email by clicking on following link," +"\n"+url);

                Transport.send(message);

            }
            catch (MessagingException e) {
                throw new RuntimeException(e);
            }
        }
 }