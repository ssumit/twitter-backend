package MiniTwitter.DataAccess;

import MiniTwitter.ConsistencyManagers.MakeCacheConsistent;
import MiniTwitter.DataStore.CachingI;
import MiniTwitter.DataStore.DataStoreI;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/14/12
 * Time: 2:04 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class OAuthDataAccess {
    public final CachingI cache;
    public final DataStoreI dataStoreI;
    public final MakeCacheConsistent makeCacheConsistent;
    public final UserDataStore userDataStore;

    @Autowired
    public OAuthDataAccess(CachingI cache, DataStoreI dataStoreI, MakeCacheConsistent makeCacheConsistent, UserDataStore userDataStore) {
        this.cache = cache;
        this.dataStoreI = dataStoreI;
        this.makeCacheConsistent = makeCacheConsistent;
        this.userDataStore = userDataStore;
    }

    public String setAndGetAccessToken(String emailid, int appID) {
        String accessToken = UUID.randomUUID().toString();//new UUID(63,1).toString();
        cache.setAndAccessToken(emailid,appID,accessToken);
        dataStoreI.setAndAccessToken(emailid,appID,accessToken);
        return accessToken;
    }

    public int registerApp(String modulus,int exp, String appName) {
        return dataStoreI.registerApp(modulus,exp,appName);
    }

    public Map<String,Object> getPublicKey(int appID) {
        return dataStoreI.getPublicKey(appID);
    }

    public boolean postTweet(String tweet, String accessToken) {
        if(!dataStoreI.getValidityOfAccessToken(accessToken))
            return false;

        String userid = dataStoreI.getUserIdByAccessToken(accessToken);
        if(userid ==null)
        return false;
        else {
            dataStoreI.insertTweet(userid, new JSONObject(userDataStore.getUserInfo(userid)).toString(), tweet);
            makeCacheConsistent.helpForInsertTweet(userid);
            return true;
        }
    }

    public List<Map<String, Object>> getAllNewsFeed(String accessToken) {
        if(!dataStoreI.getValidityOfAccessToken(accessToken))
            return null;
        String userid = dataStoreI.getUserIdByAccessToken(accessToken);
        if(userid ==null)
            return null;
        else {
            return dataStoreI.getFeed(userid,1000);
        }
    }
}
