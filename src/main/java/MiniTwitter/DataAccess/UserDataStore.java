package MiniTwitter.DataAccess;

import MiniTwitter.ConsistencyManagers.MakeCacheConsistent;
import MiniTwitter.DataStore.CachingI;
import MiniTwitter.DataStore.DataStoreI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/6/12
 * Time: 1:15 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class UserDataStore {
    public final CachingI cache;
    public final DataStoreI dataStoreI;
    public final MakeCacheConsistent makeCacheConsistent;

    @Autowired
    public UserDataStore(CachingI cache, DataStoreI dataStoreI, MakeCacheConsistent makeCacheConsistent) {
        this.cache = cache;
        this.dataStoreI = dataStoreI;
        this.makeCacheConsistent = makeCacheConsistent;
    }

    public boolean userIdExists(String userId) {
        if(cache.userIdExists(userId))
            return true;
        else {
            boolean answer = dataStoreI.userIdExists(userId);
            if (answer) {
                makeCacheConsistent.insertUserId(userId);
            }
            return answer;
        }
    }
    public boolean userEmailIdExists(String emailid) {
        if (cache.userEmailIdExists(emailid))
            return true;
        else return dataStoreI.userEmailIdExists(emailid);
    }
    public String registerUser(String userid, String username, String password, String emailid) {
        return dataStoreI.registerUser(userid,username,password,emailid);
    }
    public String getUserIdFromEmail(String emailid) {
        String string;
        if((string = cache.getUserIdFromEmail(emailid))!=null)
            return string;
        else
        return dataStoreI.getUserIdFromEmail(emailid);
    }
    public boolean checkPassword(String emailid, String password) {
        if(cache.checkPassword(emailid,password))
            return true;
        else return dataStoreI.checkPassword(emailid, password);
    }
    public HashMap<String,Object> getUserInfo(String userid) {
        HashMap<String,Object> hashMap;
        if((hashMap= cache.getUserInfo(userid))!=null)
            return hashMap;
        else
        return dataStoreI.getUserInfo(userid);
    }
    public HashMap<String,Object> getDetailedUserInfo(String userid) {
        HashMap<String,Object> hashMap;
        if((hashMap= cache.getDetailedUserInfo(userid))!=null)
            return hashMap;
        else
            return dataStoreI.getDetailedUserInfo(userid);
    }

    public boolean follow(String loggedInUserId, String targetUserId) {
        if (cache.follow(loggedInUserId,targetUserId))
            return true;
        else return dataStoreI.follow(loggedInUserId,targetUserId);
    }
    public boolean unfollow(String loggedInUserId, String targetUserId) {
        if (cache.unfollow(loggedInUserId, targetUserId))
            return true;
        else return dataStoreI.unfollow(loggedInUserId, targetUserId);
    }
    public HashMap<String,Object> getShortProfile(String userId) {
        HashMap<String,Object> hashMap;
        if((hashMap= cache.getShortProfile(userId))!=null)
            return hashMap;
        else return dataStoreI.getShortProfile(userId);
    }
    public List<Map<String, Object>> search(final String query) {
        List<Map<String,Object>> list;
        if ((list= cache.search(query))!=null)
            return list;
        else return dataStoreI.search(query);
    }
    public boolean editprofile(String username, String emailid, String password, String userid) {
        if (cache.editprofile(username,emailid,password,userid))
            return true;
        else {
            return dataStoreI.editprofile(username,emailid,password,userid);
        }
    }

    public String getPassword(String emailid) {
        return dataStoreI.getPassword(emailid);
    }

    public void markConfirmMail(String emailid) {
        dataStoreI.markConfirmMail(emailid);
    }

    public boolean emailVerified(String emailid) {
        return dataStoreI.emailVerified(emailid);
    }
    public String getFollowersList(String userid) {
        return dataStoreI.getFollowersList(userid);
    }

    public String getFollowingList(String userid) {
        return dataStoreI.getFollowingList(userid);
    }

    public List<Map<String,Object>> getAllTweets(String userid) {
        return dataStoreI.getAllTweets(userid);
    }

    public String getPasswordByUserid(String userid) {
        return dataStoreI.getPasswordByUserId(userid);
    }

    public String getEmailFromUserId(String userid) {
        return dataStoreI.getEmailFromUserId(userid);
    }
}