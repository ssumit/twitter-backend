package MiniTwitter.DataAccess;

import MiniTwitter.ConsistencyManagers.MakeCacheConsistent;
import MiniTwitter.DataStore.CachingI;
import MiniTwitter.DataStore.DataStoreI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/6/12
 * Time: 3:18 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class TweetDataStore {
    public final CachingI cache;
    public final DataStoreI dataStoreI;
    public final MakeCacheConsistent makeCacheConsistent;

    @Autowired
    public TweetDataStore(CachingI cache, DataStoreI dataStoreI, MakeCacheConsistent makeCacheConsistent) {
        this.cache = cache;
        this.dataStoreI = dataStoreI;
        this.makeCacheConsistent = makeCacheConsistent;
    }
    public List<Map<String,Object>> getFeed(String userid, Integer count) {
        List<Map<String,Object>> list;
        if ((list= cache.getFeed(userid,count))!=null)
            return list;
        else {
            list = dataStoreI.getFeed(userid,count);
            makeCacheConsistent.helpForGetFeed(userid, count, list);
            return list;
        }
    }
    public List<Map<String,Object>> getTweets(String userid, Integer count) {
        List<Map<String,Object>> list;
        if ((list= cache.getTweets(userid, count))!=null)
            return list;
        else return dataStoreI.getTweets(userid, count);
    }
    public List<Map<String,Object>> search(String query) {
        List<Map<String,Object>> list;
        if ((list= cache.searchTweet(query))!=null)
            return list;
        else return dataStoreI.searchTweet(query);
    }
    public List<Map<String, Object>> getFeedBefore(String userid, String feedId, Integer count) {
        List<Map<String,Object>> list;
        if ((list= cache.getFeedBefore(userid, feedId, count))!=null)
            return list;
        else return dataStoreI.getFeedBefore(userid, feedId, count);
    }
    public List<Map<String, Object>> getFeedAfter(String userid, String feedId) {
        List<Map<String,Object>> list;
        if ((list= cache.getFeedAfter(userid, feedId))!=null)
            return list;
        else {

            list = dataStoreI.getFeedAfter(userid, feedId);
            makeCacheConsistent.helpForGetFeedAfter(userid, feedId, list);
            return list;
        }
    }

    public List<Map<String, Object>> getTweetsBefore(String userid, String postid, Integer count) {
        List<Map<String,Object>> list;
        if ((list= cache.getTweetsBefore(userid, postid, count))!=null)
            return list;
        else return dataStoreI.getTweetsBefore(userid, postid, count);
    }

    public List<Map<String, Object>> getTweetsAfter(String userid, String postid) {
        List<Map<String,Object>> list;
        if ((list= cache.getTweetsAfter(userid, postid))!=null)
            return list;
        else return dataStoreI.getTweetsAfter(userid, postid);
    }


    public String insertTweet(String userid, String userInfo, String postContent) {
        String string;
        if((string= cache.insertTweet(userid,userInfo,postContent))!=null)
            return string;
        else {
            makeCacheConsistent.helpForInsertTweet(userid);
            return dataStoreI.insertTweet(userid,userInfo,postContent);
        }
    }

    public Map<String, Object> getPost(String postid) {
        return dataStoreI.getPost(postid);
    }

    public void retweet(String retweetingUserId, Timestamp timestamp, String sourceuserid, String sourceuser, String postcontent, String postid) {
        dataStoreI.retweet(retweetingUserId,timestamp,sourceuserid,sourceuser,postcontent,postid);
    }

    public void insertIntoFeed(Timestamp timestamp, String retweetingUserId, String sourceuser, Object postcontent, String postid, String sourceuserid) {
        dataStoreI.insertIntoFeed(timestamp,retweetingUserId,sourceuser,postcontent,postid,sourceuserid);
    }
}
