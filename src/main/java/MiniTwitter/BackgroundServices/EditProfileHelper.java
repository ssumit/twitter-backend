package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.DataStoreI;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created with IntelliJ IDEA.
 * User: jack
 * Date: 5/8/12
 * Time: 6:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class EditProfileHelper extends Thread {
    private DataStoreI db;
    private String userid, userinfo;

    public EditProfileHelper(DataStoreI db, String userid, String userinfo) {
        this.db = db;
        this.userid = userid;
        this.userinfo = userinfo;
    }

    public void run() {
        String followersList = db.getQueryForObject("select followersidlist from users where userid = ?",String.class, userid);
        String followingList, newFollowingList, newFollowersList;
        if(followersList.length()>0) for(String user: followersList.split(",")) {
            followingList = db.getQueryForObject("select followinglist from users where userid = ?",String.class, user);
            newFollowingList = updateUserInfo(followingList);
            db.doUpdate("update users set followinglist = ? where userid = ?",newFollowingList, user);
        }
        followingList = db.getQueryForObject("select followingidlist from users where userid = ?",String.class, userid);
        if(followingList.length()>0) for(String user: followingList.split(",")) {
            followersList = db.getQueryForObject("select followerslist from users where userid = ?",String.class, user);
            newFollowersList = updateUserInfo(followersList);
            db.doUpdate("update users set followerslist = ? where userid = ?",newFollowersList, user);
        }
    }

    String updateUserInfo(String userList) {
        JSONArray userArray = null;
        String newUserList = "";
        try {
            userArray = new JSONArray("["+userList+"]");
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        for(int i=0;i<userArray.length();i++) {
            JSONObject user = null;
            try {
                user = userArray.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                if (user.getString("userid").equals(userid)) {
                    newUserList += ","+userinfo;
                } else {
                    newUserList += ","+user.toString();
                }
            } catch (JSONException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if(newUserList.length()>0)
            return newUserList.substring(1);
        else return "";
    }
}
