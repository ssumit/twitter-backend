package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.DataStoreI;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 7/23/12
 * Time: 12:52 PM
 * To change this dataStoreI use File | Settings | File Templates.
 */
public class PopulateFeedHelper extends Thread {
    private DataStoreI db;
    private String userId, userInfo, postId, postContent;

    public PopulateFeedHelper(DataStoreI db, String userId, String userInfo, String postId, String postContent) {
        this.db = db;
        this.userId = userId;
        this.userInfo = userInfo;
        this.postId = postId;
        this.postContent = postContent;
    }

    @Override
    public void run() {
        String followersList = db.getQueryForObject("select followersidlist from users where userid = ?",String.class, userId);
        if(followersList.length()>0) for(String user: followersList.split(",")) {
            db.doUpdate("insert into feeds (timestamp, feeduserid, sourceuserid, sourceuser, postcontent, postid) values(now(),?,?,?,?,?)",user, userId, userInfo, postContent,postId);
        }
    }

}
