package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.CachingI;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/7/12
 * Time: 1:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class getFeedAfterUpdateRedisHelper extends Thread{
    List<Map<String, Object>> list;
    CachingI cache;
    String userId, feedId;
    public getFeedAfterUpdateRedisHelper(String userid, String feedid,  CachingI cache, List<Map<String, Object>> list) {
        this.list = list;
        this.cache = cache;
        this.userId = userid;
        this.feedId = feedid;
    }
    @Override
    public void run() {
        cache.insertFeedAfterInCache(userId, list,feedId);
    }


}
