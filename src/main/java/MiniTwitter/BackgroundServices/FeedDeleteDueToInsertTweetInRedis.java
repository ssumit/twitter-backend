package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.CachingI;
import MiniTwitter.DataStore.DataStoreI;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/8/12
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class FeedDeleteDueToInsertTweetInRedis extends Thread{
    String userId;
    DataStoreI db;
    CachingI cache;
    public FeedDeleteDueToInsertTweetInRedis(String userid, DataStoreI db, CachingI cache) {
        this.userId = userid;
        this.db = db;
        this.cache = cache;
    }
    @Override
    public void run(){
        String followersList = db.getQueryForObject("select followersidlist from users where userid = ?", String.class, userId);
        cache.feedDeleteDueToInsertTweet(followersList);
    }
}
