package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.CachingI;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/8/12
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class FeedUpdateRedisHelper extends Thread{
    String userId;
    CachingI cache;
    List<Map<String,Object>> list;
    Integer count;
    public FeedUpdateRedisHelper(String userId, CachingI cache, Integer count, List<Map<String, Object>> list) {
        this.userId = userId;
        this.cache = cache;
        this.list = list;
        this.count = count;
    }
    @Override
    public void run(){
        cache.feedUpdateInCache(userId,list,count);
    }
}
