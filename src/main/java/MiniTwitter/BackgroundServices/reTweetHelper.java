package MiniTwitter.BackgroundServices;

import MiniTwitter.DataStore.DataStoreI;

import java.sql.Timestamp;

/**
 * Created with IntelliJ IDEA.
 * User: jack
 * Date: 14/8/12
 * Time: 11:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class reTweetHelper extends Thread {
    private DataStoreI db;
    private Timestamp timestamp;
    private String retweetingUserId, sourceuserid, sourceuser, postcontent, postid;

    public reTweetHelper(DataStoreI db, String retweetingUserId, Timestamp timestamp, String sourceuserid, String sourceuser, String postcontent, String postid) {
        this.db = db;
        this.retweetingUserId = retweetingUserId;
        this.timestamp = timestamp;
        this.sourceuserid = sourceuserid;
        this.sourceuser = sourceuser;
        this.postcontent = postcontent;
        this.postid = postid;
    }

    @Override
    public void run() {
        String followersList = db.getQueryForObject("select followersidlist from users where userid = ?",String.class, retweetingUserId);
        if(followersList.length()>0) for(String user: followersList.split(",")) {
            db.doUpdate("delete from feeds where feeduserid = ? and postid = ?",user,postid);
            db.doUpdate("insert into feeds (timestamp, feeduserid, sourceuserid, sourceuser, postcontent, postid) values(?,?,?,?,?,?)",timestamp,user, sourceuserid, sourceuser, postcontent,postid);
        }
    }
}
