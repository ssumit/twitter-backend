package MiniTwitter.ConsistencyManagers;

import MiniTwitter.BackgroundServices.FeedDeleteDueToInsertTweetInRedis;
import MiniTwitter.BackgroundServices.FeedUpdateRedisHelper;
import MiniTwitter.BackgroundServices.getFeedAfterUpdateRedisHelper;
import MiniTwitter.DataStore.CachingI;
import MiniTwitter.DataStore.DataStoreI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/6/12
 * Time: 6:55 PM
 * To change this template use File | Settings | File Templates.
 */

@Service
public class MakeCacheConsistent  {
    public final CachingI cache;
    public final DataStoreI db;

    @Autowired
    public MakeCacheConsistent(CachingI cache, DataStoreI db) {
        this.cache = cache;
        this.db = db;
    }

    public void insertUserId(String userId) {
        cache.insertUserIdConsistentency(userId);
    }

    public void helpForGetFeedAfter(String userid, String feedId, List<Map<String, Object>> list) {
        new getFeedAfterUpdateRedisHelper(userid ,feedId, cache,list).start();
    }

    public void helpForGetFeed(String userid, Integer count, List<Map<String, Object>> list) {
        new FeedUpdateRedisHelper(userid , cache,count, list).start();
    }

    public void helpForInsertTweet(String userid) {
        cache.insertTweetConsistency(userid);
        new FeedDeleteDueToInsertTweetInRedis(userid,db, cache).start();
    }
}
