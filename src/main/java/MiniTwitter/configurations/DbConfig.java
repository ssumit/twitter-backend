package MiniTwitter.configurations;

import MiniTwitter.Constants;
import MiniTwitter.DataStore.JDBCWrapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;


/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 18/7/12
 * Time: 5:09 AM
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class DbConfig {
    @Bean
    JDBCWrapper getJDBCWrapper(){
        return new JDBCWrapper();
    }
    @Bean
    JedisPool getJedisPool(){
        JedisPool jedisPool = new JedisPool(new JedisPoolConfig(), Constants.LOCALHOST);
        Jedis jedis = jedisPool.getResource();
        jedis.flushAll();
        jedisPool.getResource();
        return jedisPool;
    }

}

