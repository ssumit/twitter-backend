package MiniTwitter.controllers;

import MiniTwitter.Constants;
import MiniTwitter.services.JavaMailingService;
import MiniTwitter.services.MiscUtilities;
import MiniTwitter.services.Tweets;
import MiniTwitter.services.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;


/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 7/17/12
 * Time: 5:12 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class UserController {
    public final Users users;
    public final Tweets tweets;
    public final MiscUtilities miscUtilities;

    @Autowired
    public UserController(Users users, Tweets tweets, MiscUtilities miscUtilities) {this.users = users; this.tweets = tweets;
        this.miscUtilities = miscUtilities;
    }

    @RequestMapping(value = "/register/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> register(@RequestParam String userid, @RequestParam String username, @RequestParam String password, @RequestParam String emailid, @RequestParam String recaptcha_challenge_field, @RequestParam String recaptcha_response_field, HttpSession session) throws NoSuchAlgorithmException, IOException {
        HashMap<String,String> response = new HashMap<String, String>();
        if(emailid==null || emailid==""|| !(new MiscUtilities.EmailValidator().validate(emailid))){
            response.put(Constants.RESPONSE_MESSAGE, Constants.INVALID_EMAIL);
            response.put(Constants.RESPONSE_DISPLAY,Constants.REGISTER);
            return response;
        }
        if(!miscUtilities.isCaptchaValid(recaptcha_challenge_field,recaptcha_response_field)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.CAPTCHA_WRONGLY_ANSWERED);
            response.put(Constants.RESPONSE_DISPLAY,Constants.REGISTER);
            return response;
        }
        if(users.userEmailIdExists(emailid)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.EMAIL_AlREADY_REGISTERED);
            response.put(Constants.RESPONSE_DISPLAY,Constants.REGISTER);
            return response;
        }
        if(users.userIdExists(userid)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.USERID_ALREADY_REGISTERED);
            response.put(Constants.RESPONSE_DISPLAY,Constants.REGISTER);
            return response;
        }
        String newuserid = users.registerUser(userid,username,password,emailid);

        if(newuserid!=null) {
            String url = users.getURL(emailid);
            miscUtilities.copyFile(Constants.DEFAULT_PIC, Constants.PIC_BASE + userid);
            new JavaMailingService(emailid, "http://localhost/frontend/confirm?emailid=" + emailid + "&hash=" + url).start();
            response.put(Constants.RESPONSE_MESSAGE, Constants.REGISTRATION_SUCCESS);
            response.put("userid", userid);
            response.put(Constants.RESPONSE_DISPLAY, Constants.RESPONSE_FEED);
            return response;
        } else {
            response.put(Constants.RESPONSE_MESSAGE, Constants.REGISTRATION_FAILED);
            response.put(Constants.RESPONSE_DISPLAY,Constants.REGISTER);
            response.put("email",emailid);
            response.put(Constants.RESPONSE_PASSWORD,password);
            return response;
        }
    }

    @RequestMapping(value = "/useridexists/") @ResponseBody
    HashMap<String,Object> useridexists(@RequestParam String userid) {
        if(users.userIdExists(userid))
            return new HashMap<String, Object>() {{ put(Constants.RESPONSE_MESSAGE,Constants.USERID_EXISTS); }};
        else
            return new HashMap<String, Object>() {{ put(Constants.RESPONSE_MESSAGE,Constants.USERID_FREE); }};
    }

    @RequestMapping(value = "/login/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> login(@RequestParam String emailid, @RequestParam String password, HttpSession session) {
        HashMap<String,String> response = new HashMap<String, String>();
        if(session.getAttribute("userid")!=null) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.ALREADY_LOGGED_IN);
            response.put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_FEED);
            return response;
        }
        if(!users.userEmailIdExists(emailid)) {
            response.put(Constants.RESPONSE_MESSAGE, Constants.EMAIL_NOT_REGISTERED);
            response.put(Constants.RESPONSE_DISPLAY, Constants.RESPONSE_LOGIN);
            return response;
        }
        if(!users.emailVerified(emailid)) {
            response.put(Constants.RESPONSE_MESSAGE, Constants.EMAIL_NOT_VERIFIED);
            response.put(Constants.RESPONSE_DISPLAY, Constants.RESPONSE_LOGIN);
            return response;
        }
        if(users.checkPassword(emailid,password)) {
            users.login(users.getUserIdFromEmail(emailid),session);
            response.put(Constants.RESPONSE_MESSAGE,Constants.LOGIN_SUCCESS);
            response.put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_FEED);
            return response;
        } else {
            response.put(Constants.RESPONSE_MESSAGE,Constants.WRONG_PASSWORD);
            response.put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_LOGIN);
            return response;
        }
    }

    @RequestMapping(value = "/editprofile/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,Object> editprofile(@RequestParam String username, @RequestParam String emailid, @RequestParam String currentpassword, @RequestParam String password, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        String currentPasswordInDB = users.getPasswordByUserid(userid);
        String message = "";
        if(emailid==null || emailid==""|| !(new MiscUtilities.EmailValidator().validate(emailid))){
            message += Constants.INVALID_EMAIL+", email not updated. ";
            emailid = users.getEmailFromUserId(userid);
        } else if(users.userEmailIdExists(emailid)) {
            message += Constants.EMAIL_AlREADY_REGISTERED+", email not updated. ";
            emailid = users.getEmailFromUserId(userid);
        }
        if(!currentpassword.equals("")&&!currentpassword.equals(currentPasswordInDB)) {
            message += Constants.WRONG_PASSWORD+", password not updated. ";
            password = currentPasswordInDB;
        }
        if(password.equals(""))
            password = currentPasswordInDB;
        HashMap<String,Object> response = new HashMap<String, Object>();
        if(users.editprofile(username,emailid,password,userid)) {
            response.put(Constants.RESPONSE_MESSAGE,message+Constants.SAVED_WILL_PROPAGATE_SOON);
            return response;
        } else {
            return new HashMap<String, Object>() {{
                put(Constants.RESPONSE_MESSAGE,Constants.TRY_AGAIN);
            }};
        }
    }

    @RequestMapping(value = "/editpicture/", method = RequestMethod.POST) @ResponseBody
    HashMap<String, Object> uploadPicture(@RequestParam final MultipartFile picture, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        File pictureFile = new File("/var/www/frontend/avatar/"+userid);
        try {
            BufferedImage image = ImageIO.read(picture.getInputStream());
            BufferedImage newimage = new BufferedImage(100,100,BufferedImage.TYPE_INT_RGB);
            Graphics g = newimage.createGraphics();
            g.drawImage(image.getScaledInstance(100,100, Image.SCALE_FAST),0,0,null);
            ImageIO.write(newimage,"jpg",pictureFile);
        } catch (IOException e) {
            return new HashMap<String, Object>() {{
                put(Constants.RESPONSE_MESSAGE,"Problem in uploading file, try later or someother picture");
            }};
        }
        return new HashMap<String, Object>() {{
            put(Constants.RESPONSE_MESSAGE,"file uploaded successfully");
        }};
    }

    @RequestMapping(value = "/logout/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> logout(HttpSession session) {
        if(!users.checkUserLogin(session))
            return new HashMap<String, String>() {{ put("message",Constants.LOGIN_TO_LOGOUT); }};
        users.logout(session);
        return new HashMap<String, String>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.LOGGED_OUT);
            put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_LOGIN);
        }};
    }
    @RequestMapping(value = "/profile/{userid}", method=RequestMethod.GET) @ResponseBody
    HashMap<String,Object> getProfile(@PathVariable("userid") String targetUserId, HttpSession session) {
        if(!users.userIdExists(targetUserId)) {
            return new HashMap<String, Object>() {{
                put(Constants.RESPONSE_MESSAGE,Constants.USERID_DOES_NOT_EXIST);
            }};
        }
        return users.getProfile(targetUserId);
    }

    @RequestMapping(value = "/shortprofile/{userid}", method = RequestMethod.GET) @ResponseBody
    HashMap<String,Object> getShortProfile(@PathVariable String userid, HttpSession session) {
        if(!users.userIdExists(userid)) {
            return new HashMap<String, Object>() {{ put(Constants.RESPONSE_MESSAGE,Constants.USERID_DOES_NOT_EXIST); }};
        }
        return users.getShortProfile(userid);
    }
    @RequestMapping("/myprofile/") @ResponseBody
    HashMap<String,Object> myprofile(HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        return users.getProfile(users.getLoggedInUserId(session));
    }

    @RequestMapping(value = "/follow/{userid}", method=RequestMethod.GET) @ResponseBody
    HashMap<String,Object> follow(@PathVariable("userid") String targetUserId,HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        HashMap<String,Object> response = new HashMap<String,Object>();
        if(users.getLoggedInUserId(session).equals(targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE, Constants.FOLLOW_YOURSELF);
        } else if(!users.userIdExists(targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.USERID_DOES_NOT_EXIST);
        } else if(!users.follow(users.getLoggedInUserId(session), targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.ALREADY_FOLLOW);
        } else {
            response.put(Constants.RESPONSE_MESSAGE,Constants.SUCCESSFULLY_FOLLOWED);
        }
        response.put("myProfile",users.getProfile(users.getLoggedInUserId(session)));
        response.put("currentProfile",users.getProfile(targetUserId));
        return response;
    }

    @RequestMapping(value = "/unfollow/{userid}", method=RequestMethod.GET) @ResponseBody
    HashMap<String,Object> unfollow(@PathVariable("userid") String targetUserId,HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        HashMap<String,Object> response = new HashMap<String,Object>();
        if(users.getLoggedInUserId(session).equals(targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.UNFOLLOW_YOURSELF);
        } else if(!users.userIdExists(targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.USERID_DOES_NOT_EXIST);
        } else if(!users.unfollow(users.getLoggedInUserId(session), targetUserId)) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.ALREADY_UNFOLLOW);
        } else {
            response.put(Constants.RESPONSE_MESSAGE,Constants.SUCCESSFULLY_UNFOLLOWED);
        }
        response.put("myProfile",users.getProfile(users.getLoggedInUserId(session)));
        response.put("currentProfile",users.getProfile(targetUserId));
        return response;
    }
    @ExceptionHandler(Exception.class)
    public HashMap<String,String> handleException(Exception exception){
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_SOMETHING_WENT_WRONG);
        return hashMap;
    }
}
