package MiniTwitter.controllers;

import MiniTwitter.Constants;
import MiniTwitter.services.MiscUtilities;
import MiniTwitter.services.OAuthService;
import MiniTwitter.services.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/13/12
 * Time: 5:48 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class MyOAUTHController {
    public final MiscUtilities miscUtilities;
    public final Users users;
    public final OAuthService oAuthService;
    @Autowired
    public MyOAUTHController(MiscUtilities miscUtilities, Users users, OAuthService oAuthService) {
        this.miscUtilities = miscUtilities;
        this.users = users;
        this.oAuthService = oAuthService;
    }

    @RequestMapping(value = "/restapi/register", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> register(@RequestParam String AppName,@RequestParam String modulus, @RequestParam int exp) throws Exception {
        HashMap<String,String> response = new HashMap<String, String>();
        String Appid = new Integer(miscUtilities.registerApp(modulus,exp, AppName)).toString();
        String msg = miscUtilities.encrypt(Appid, modulus,exp);
        response.put(Constants.RESPONSE_APPID,msg);
        response.put(Constants.RESPONSE_APPID+" encrypted",Appid);
        return response;
    }

    @RequestMapping(value = "/restapi/login", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> login(@RequestParam String emailid, @RequestParam String password,@RequestParam String callbackURL,@RequestParam int appID) throws Exception {
        HashMap<String,String> response = new HashMap<String, String>();
        if(!users.checkPassword(emailid,password)){
            response.put(Constants.RESPONSE_STATUS,Constants.RESPONSE_INVALID);
            return response;
        }
//        callbackURL = miscUtilities.decrypt(callbackURL, miscUtilities.getPublicKey(appID));
        String accessToken = miscUtilities.setAndGetAccessToken(emailid,appID);
        response.put(Constants.RESPONSE_CALLBACK, callbackURL);
        response.put(Constants.RESPONSE_EMAILID, emailid);
        response.put(Constants.RESPONSE_PASSWORD, password);
        response.put(Constants.RESPONSE_ACCESSTOKEN,accessToken);
        return response;
    }
    @RequestMapping(value = "/restapi/tweet", method = RequestMethod.POST) @ResponseBody
    HashMap<String,String> postTweet(@RequestParam String tweet, @RequestParam String accessToken){
        HashMap<String,String> response = new HashMap<String, String>();
        if(oAuthService.postTweet(tweet,accessToken))
        response.put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
        else response.put(Constants.RESPONSE_MESSAGE,Constants.ANON_ERROR);
        response.put(Constants.RESPONSE_ACCESSTOKEN,accessToken);
        response.put(Constants.RESPONSE_TWEET,tweet);
        return response;
    }
    @RequestMapping(value = "/restapi/allnewsfeed", method = RequestMethod.GET) @ResponseBody
    HashMap<String,Object> getFeeds(@RequestParam String accessToken){
        HashMap<String,Object> response = new HashMap<String, Object>();
        List<Map<String,Object>> feeds = oAuthService.getAllNewsFeed(accessToken);
        if(feeds!=null)
            response.put(Constants.RESPONSE_FEEDS,feeds);
        else
            response.put(Constants.RESPONSE_FEEDS,Constants.RESPONSE_INVALID);
        return response;
    }
    @ExceptionHandler(Exception.class)
    public HashMap<String,String> handleException(Exception exception){
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_SOMETHING_WENT_WRONG);
        return hashMap;
    }

}
