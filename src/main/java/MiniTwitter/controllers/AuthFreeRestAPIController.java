package MiniTwitter.controllers;

import MiniTwitter.Constants;
import MiniTwitter.services.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/15/12
 * Time: 9:04 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class AuthFreeRestAPIController {
    public final Users users;

    @Autowired
    public AuthFreeRestAPIController(Users users) {
        this.users = users;
    }

    @RequestMapping(value = "/followerslist", method= RequestMethod.GET) @ResponseBody
    HashMap<String,String> getFollowersList(@RequestParam String userid) {
        HashMap<String,String> response = new HashMap<String, String>();
        response.put("followers list","["+users.getFollowersList(userid)+"]");
        return response;
    }

    @RequestMapping(value = "/followinglist", method= RequestMethod.GET) @ResponseBody
    HashMap<String,String> getFollowingList(@RequestParam String userid) {
        HashMap<String,String> response = new HashMap<String, String>();
        response.put("following list","["+users.getFollowingList(userid)+"]");
        return response;
    }

    @RequestMapping(value = "/alltweets", method= RequestMethod.GET) @ResponseBody
    HashMap<String,Object> getTweets(@RequestParam String userid) {
        HashMap<String,Object> response = new HashMap<String, Object>();
        response.put("tweet list",users.getAllTweets(userid));
        return response;
    }
    @ExceptionHandler(Exception.class)
    public HashMap<String,String> handleException(Exception exception){
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_SOMETHING_WENT_WRONG);
        return hashMap;
    }
}
