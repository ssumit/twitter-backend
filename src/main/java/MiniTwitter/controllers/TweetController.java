package MiniTwitter.controllers;

import MiniTwitter.Constants;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import MiniTwitter.services.Tweets;
import MiniTwitter.services.Users;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: jack
 * Date: 23/7/12
 * Time: 3:49 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
public class TweetController {
    public final Users users;
    public final Tweets tweets;
    private Integer pageTweetCount = 10;

    @Autowired
    public TweetController(Users users,Tweets tweets) {
        this.users = users;
        this.tweets = tweets;
    }

    @RequestMapping("/feed/") @ResponseBody
    HashMap<String,Object> feeds(HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        HashMap<String,Object> response = new HashMap<String,Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
            put(Constants.RESPONSE_DISPLAY, Constants.RESPONSE_FEED);
        }};
        response.put(Constants.RESPONSE_TWEETS,tweets.getFeed(userid, pageTweetCount));
        return response;
    }

    @RequestMapping("/feed/before") @ResponseBody
    HashMap<String,Object> feedsBefore(@RequestParam String feedId, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        HashMap<String,Object> response = new HashMap<String,Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
            put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_FEED);
        }};
        response.put(Constants.RESPONSE_TWEETS,tweets.getFeedBefore(userid,feedId,pageTweetCount));
        return response;
    }

    @RequestMapping("/feed/after") @ResponseBody
    HashMap<String,Object> feedsAfter(@RequestParam String feedId, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        HashMap<String,Object> response = new HashMap<String,Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
            put(Constants.RESPONSE_DISPLAY,Constants.RESPONSE_FEED);
        }};
        response.put(Constants.RESPONSE_TWEETS,tweets.getFeedAfter(userid,feedId));
        return response;
    }

    @RequestMapping("/tweets/before") @ResponseBody
    HashMap<String,Object> tweetsBefore(@RequestParam String userid, @RequestParam String postid, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        HashMap<String,Object> response = new HashMap<String,Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
            put(Constants.RESPONSE_DISPLAY,Constants.PROFILE);
        }};
        response.put(Constants.RESPONSE_TWEETS,tweets.getTweetsBefore(userid, postid, pageTweetCount));
        return response;
    }

    @RequestMapping("/tweets/after") @ResponseBody
    HashMap<String,Object> tweetsAfter(@RequestParam String userid, @RequestParam String postid, HttpSession session) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        HashMap<String,Object> response = new HashMap<String,Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.GOT_TWEETS);
            put(Constants.RESPONSE_DISPLAY,Constants.PROFILE);
        }};
        response.put(Constants.RESPONSE_TWEETS,tweets.getTweetsAfter(userid, postid));
        return response;
    }


    @RequestMapping(value = "/post/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,Object> postTweet(HttpSession session, @RequestParam String postcontent) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        HashMap<String,Object> response = new HashMap<String,Object>();
        if(postcontent.length()>140) {
            response.put("message","Tweet should be less than 140 characters");
            return response;
        }
        String tweetid = tweets.insertTweet(userid, new JSONObject(users.getUserInfo(userid)).toString(),postcontent);
        if(tweetid==null) {
            response.put(Constants.RESPONSE_MESSAGE,Constants.POST_TWEET_FAILED);
            response.put("postcontent",postcontent);
            return response;
        }
        response.put(Constants.RESPONSE_MESSAGE,Constants.POST_TWEET_SUCCESS);
        response.put("tweetid",tweetid);
        response.put("postcontent", postcontent);
        return response;
    }

    @RequestMapping(value = "/retweet/", method = RequestMethod.POST) @ResponseBody
    HashMap<String,Object> reTweet(HttpSession session, @RequestParam String postid) {
        if(!users.checkUserLogin(session))
            return users.loginResponse;
        String userid = users.getLoggedInUserId(session);
        tweets.retweet(userid,postid);
        return new HashMap<String, Object>() {{
            put(Constants.RESPONSE_MESSAGE,Constants.POST_RETWEET);
        }};
    }
    @ExceptionHandler(Exception.class)
    public HashMap<String,String> handleException(Exception exception){
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_SOMETHING_WENT_WRONG);
        return hashMap;
    }
}
