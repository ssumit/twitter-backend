package MiniTwitter.controllers;

import MiniTwitter.Constants;
import MiniTwitter.services.MiscUtilities;
import MiniTwitter.services.Tweets;
import MiniTwitter.services.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/9/12
 * Time: 9:31 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class DefaultController {
    public final Users users;
    public final Tweets tweets;
    public final MiscUtilities miscUtilities;

    @Autowired
    public DefaultController(Users users, Tweets tweets, MiscUtilities miscUtilities) {
        this.users = users;
        this.tweets = tweets;
        this.miscUtilities = miscUtilities;
    }
    @RequestMapping(value = "/usersearch/", method = RequestMethod.POST) @ResponseBody
    List<Map<String, Object>> usersearch(@RequestParam final String query) {
        return users.search(query);
    }
    @RequestMapping(value = "/search/", method=RequestMethod.POST) @ResponseBody
    HashMap<String,Object> search(@RequestParam final String query,HttpSession session) {
        HashMap<String,Object> response = new HashMap<String, Object>() {{ put("query",query); }};
        response.put(Constants.RESPONSE_USERS, users.search(query));
        response.put(Constants.RESPONSE_TWEETS,tweets.search(query));
        return response;
    }

    @RequestMapping(value = "/confirm", method=RequestMethod.POST)@ResponseBody
    HashMap<String,Object> confirmEmail(@RequestParam String emailid,@RequestParam String hash) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        HashMap<String,Object> response = new HashMap<String, Object>();
        if(miscUtilities.confirmEmail(emailid, hash))  {
        response.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_EMAIL_CONFIRMED);
            miscUtilities.markConfirmMail(emailid);
        }
        else
        response.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_TRY_AGAIN);
        response.put(Constants.RESPONSE_HASH,hash);
        return response;
    }
    @ExceptionHandler(Exception.class)
    public HashMap<String,String> handleException(Exception exception){
        HashMap<String,String> hashMap = new HashMap<String, String>();
        hashMap.put(Constants.RESPONSE_MESSAGE,Constants.RESPONSE_SOMETHING_WENT_WRONG);
        return hashMap;
    }
}
