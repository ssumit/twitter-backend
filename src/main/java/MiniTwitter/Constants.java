package MiniTwitter;

/**
 * Created with IntelliJ IDEA.
 * User: ssumit
 * Date: 8/13/12
 * Time: 1:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
     public static final String INVALID_EMAIL = "Invalid Email";
    public static final String CAPTCHA_WRONGLY_ANSWERED = "Captcha Wrongly Answered.";
    public static final String REGISTER = "register";
    public static final String EMAIL_AlREADY_REGISTERED = "Emailid already registered";
    public static final String USERID_ALREADY_REGISTERED = "UserId already registered";
    public static final String REGISTRATION_SUCCESS = "Registration success, Verify email to login";
    public static final String REGISTRATION_FAILED = "Registration failed";
    public static final String USERID_EXISTS = "UserId Exists";
    public static final String USERID_FREE = "UserId Free";
    public static final String ALREADY_LOGGED_IN = "Already Logged In";
    public static final String EMAIL_NOT_REGISTERED = "Email not registered";
    public static final String LOGIN_SUCCESS = "Login success";
    public static final String WRONG_PASSWORD = "Wrong password";
    public static final String SAVED_WILL_PROPAGATE_SOON = "Successfully saved, will take some time to propagate the changes";
    public static final String TRY_AGAIN = "Couldn't save profile, try again later";
    public static final String LOGIN_TO_LOGOUT = "You've to be logged in to logout";
    public static final String LOGGED_OUT = "Successfully logged out";
    public static final String USERID_DOES_NOT_EXIST = "Userid does not exist";
    public static final String PROFILE = "profile";
    public static final String GOT_TWEETS = "Got tweets";
    public static final String POST_TWEET_FAILED ="Posting tweet failed";
    public static final String POST_TWEET_SUCCESS ="Tweet posted successfullly";
    public static final String FOLLOW_YOURSELF ="Unnecessary.. You follow yourself by default";
    public static final String ALREADY_FOLLOW ="You already follow this user.";
    public static final String SUCCESSFULLY_FOLLOWED ="Successfully followed.";
    public static final String UNFOLLOW_YOURSELF ="You cannot unfollow yourself";
    public static final String ALREADY_UNFOLLOW ="You are not following that user.";
    public static final String SUCCESSFULLY_UNFOLLOWED ="Successfully unfollowed.";
    public static final String USER_NOT_LOGGEDIN ="User not logged in";
    public static final String OAUTH_LOGIN = "/restapi/login";
    public static final String RECAPTCHA_PRIVATE_KEY ="6LdFFdUSAAAAANEjE2eVVzlUuAvA1r6Z9EJD8HRN";
    public static final String LOCALHOST = "127.0.0.1";
    public static final String EMAIL_PATTERN ="^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String PSQL_URL = "jdbc:postgresql://localhost/twitterdb";
    public static final String PSQL_DRIVER_CLASS = "org.postgresql.Driver";
    public static final String PSQL_USERNAME = "twitter";
    public static final String PSQL_PASSWORD = "twitter";

    public static final String POST_RETWEET = "Retweeted will take some time to propagate";

    public static final String public_key_mod ="106394877608018766537720801416655991345106535990850729605963854419450103716730599362154190537257597233014065015311499176359112816419965961469419756050290964343366687334245741905264407605904082573446954295309549250335299907317631410981650257400135070254491184426528002396792285738067623733919575203674519111607";
    public static final String public_key_exp ="65537";
    public static final String private_key_mod ="106394877608018766537720801416655991345106535990850729605963854419450103716730599362154190537257597233014065015311499176359112816419965961469419756050290964343366687334245741905264407605904082573446954295309549250335299907317631410981650257400135070254491184426528002396792285738067623733919575203674519111607";
    public static final String private_key_exp ="93452875925135424199214152822829884519905667703943150430555400455554041540711307387924469815176672486052987665080892474283417447811394183010927388452858662476662035140656394215689698124107608633883372820489737017958964840619312664946378654697306323847353967028752216676895722113336574673751436796112559087393";

    public static final String ANON_ERROR = "something went wrong";
    public static final String EMAIL_NOT_VERIFIED = "Email not verified";
    public static final String DEFAULT_PIC="/var/www/frontend/default.png";
    public static final String PIC_BASE = "/var/www/frontend/avatar/";
    public static final String RESPONSE_MESSAGE="message";
    public static final String RESPONSE_TWEET="tweet";
    public static final String RESPONSE_ACCESSTOKEN="accesstoken";
    public static final String RESPONSE_PASSWORD="password";
    public static final String RESPONSE_FEED = "feed";
    public static final String RESPONSE_LOGIN = "login";
    public static final String RESPONSE_CALLBACK = "callback";
    public static final String RESPONSE_EMAILID = "emailid";
    public static final String RESPONSE_APPID = "Appid";
    public static final String RESPONSE_STATUS = "status";
    public static final String RESPONSE_INVALID = "invalid";
    public static final String RESPONSE_EMAIL_CONFIRMED="email confirmed";
    public static final String RESPONSE_TRY_AGAIN="Try Again";
    public static final String RESPONSE_HASH="hash";
    public static final String RESPONSE_USERS="users";
    public static final String RESPONSE_TWEETS="tweets";
    public static final String RESPONSE_SOMETHING_WENT_WRONG="Something went wrong";
    public static final String RESPONSE_DISPLAY="display";
    public static final String RESPONSE_FEEDS="feeds";
    public static final String EMAIL_VERIFICATION_USERID="dontreplybackminitwitter";
    public static final String EMAIL_VERIFICATION_PASSWORD="DEFAULTPASSWORD";
}

